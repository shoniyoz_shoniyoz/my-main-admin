import {createRouter, createWebHistory} from 'vue-router'

function authGuard(to, from, next) {
    if (localStorage.getItem("token")) {
        next();
    } else {
        next("/login");
    }
}

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/login",
            name: "login",
            meta: {layout: "default"},
            component: () => import("../pages/login/Index.vue"),
        },
        {
            path: '/',
            name: 'home',
            meta: {layout: "main"},
            component: () => import('../pages/home/Index.vue'),
            beforeEnter: authGuard
        },
        {
            path: '/cinema',
            name: 'cinema',
            meta: {layout: "main"},
            component: () => import('../pages/cinema/Index.vue'),
            beforeEnter: authGuard
        },
        {
            path: '/club',
            name: 'club',
            meta: {layout: "main"},
            component: () => import('../pages/club/Index.vue'),
            beforeEnter: authGuard
        },
        {
            path: '/club-category',
            name: 'category',
            meta: {layout: "main"},
            component: () => import('../pages/club-category/Index.vue'),
            beforeEnter: authGuard
        }
    ]
})

export default router
