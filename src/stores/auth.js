import { defineStore } from "pinia";
import $axios from "@/plugins/axios";
import {removeItem, setItem} from "../helpers/persistaneStorage"

export const useAuth = defineStore("auth", {
    state: () => ({
        user: null,
    }),
    actions: {
        login(user) {
            return new Promise((resolve, reject) => {
                $axios.post('/auth/login', user)
                    .then((respose) => {
                        resolve(respose)
                        removeItem('token')
                        setItem("token", respose.data.token)
                        $axios.defaults.headers.common.authorization = `Bearer ${respose.data.token}`;
                    })
                    .catch((error) => {
                        reject(error)
                    })
            })

        },
        logout() {
            removeItem('token')
        },
    },
});
